package com.citi.trading;
import static org.junit.Assert.assertEquals;

import java.util.function.Consumer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import com.citi.trading.Investor;
import com.citi.trading.Market;
import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;

public class InvestorTest {
	public static Investor i;

	public static class MockMarket implements OrderPlacer {
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			callback.accept(order);
		}
	}

	@Before
	public void setup()
	{
		OrderPlacer m=new MockMarket();
		i=new Investor(10000,m);
		i.buy("MRK", 100, 60);
	}

	@Test
	public void testCash() throws Exception {
		assertThat(i.getCash(), closeTo(4000, 0.00001));
	}

	public void testPortFolio() throws Exception {
		assertThat(i.getPortfolio(), hasEntry("MRK",100));
	}


//	public void test() throws Exception {
//		OrderPlacer m=new MockMarket();
//		Investor investor=new Investor(10000,m);
//		investor.buy("MRK", 100, 60);
//		investor.getCash();
//		assertThat(investor.getCash(), closeTo(4000, 0.00001));
//
//	}

//	public void testPortfolio() {
//		investor.buy("MRK", 100, 60);
//	    assertThat(investor.getPortfolio(), equalTo());
//	}

//	public static void main(String[] args) {
//
//		InvestorTest it=new InvestorTest();
//		it.test();
//	}

}
